$(document).ready(function () {
    $("span[data-label-for]").prop("data-toggle", "tooltip");
    $("span[data-label-for]").prop("title", "Kopierat till urklippsminnet");
    $("span[data-label-for]").tooltip({trigger: "manual", delay: {hide: 1000}});


    $(document).on('shown.bs.tooltip', function (e) {
        setTimeout(function () {
            $(e.target).tooltip('hide');
        }, 1000);
    });

    $("span[data-label-for]").click(function () {
        clipboard.copy($("#" + $(this).attr("data-label-for")).text());
        $(this).tooltip("show");
    });
});