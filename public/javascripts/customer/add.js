$(document).ready(function() {
    $("#addCorporateCustomer").hide();

    $('input[type=radio][name=customerType]').change(function() {
        if (this.value == '0') {
            $("#addCorporateCustomer input").prop("required", false);
            $("#addPrivateCustomer input").prop("required", true);
            $("#addCorporateCustomer").slideUp(function() {
                $("#addPrivateCustomer").slideDown();
            });
        }
        else if (this.value == '1') {
            $("#addPrivateCustomer input").prop("required", false);
            $("#addCorporateCustomer input").prop("required", true);
            $("#addPrivateCustomer").slideUp(function() {
                $("#addCorporateCustomer").slideDown();
            });
        }
    });
});