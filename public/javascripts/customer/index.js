var LoaderSettings = {
    tableBody: "tbody",
    ajaxUrl: "/customer/rest/get/all/json/",
    ajaxSearchUrl: function (search, type) {
        return "/customer/rest/search/json?search=" + search + "&type=" + type;
    },

    btnValue: 0,

    fillFunction: function (customers) {
        jQuery.each(customers, function (index, customer) {
            var row = $("<tr></tr>");
            row.attr("data-href", "/customer/" + customer.id + "/show").attr("class", "clickable-row");
            $("tbody").append(row);


            if (customer.customerType == 0) {
                LoaderSettings.fillPrivateCustomer(row, customer);
            } else if (customer.customerType == 1) {
                LoaderSettings.fillCorporateCustomer(row, customer);
            }

        });
    },

    fillPrivateCustomer: function (element, customer) {
        element.append($("<td></td>").text(customer.id));
        element.append($("<td></td>").text(customer.privateCustomer.firstname + " " + customer.privateCustomer.lastname));
        element.append($("<td></td>").text(customer.phoneNumber));
        element.append($("<td></td>").text(customer.email));
        element.append($("<td></td>").text("Privat"));
    },

    fillCorporateCustomer: function (element, customer) {
        element.append($("<td></td>").text(customer.id));
        element.append($("<td></td>").text(customer.corporateCustomer.companyName));
        element.append($("<td></td>").text(customer.phoneNumber));
        element.append($("<td></td>").text(customer.email));
        element.append($("<td></td>").text("Företag"));
    },


    tblLoader: null
};

$(document).ready(function () {
    $("table").show();
    var tableLoader = new TableLoader();
});

