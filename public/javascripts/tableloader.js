var TableLoader = function () {
    $("table").show();
    LoaderSettings.tblLoader = this;

    this.doneZeroLenEvent = 0;

    this.loadData();
    this.bindBtnClick();
    this.bindSearchEvents();
}

TableLoader.prototype.loadData = function () {
    $("tbody").fadeOut(200, function () {
        $.ajax(LoaderSettings.ajaxUrl + LoaderSettings.btnValue)
            .done(function (items) {
                LoaderSettings.tblLoader.insertData(items);
                $("tbody").fadeIn();
            })
            .fail(function () {

            })
    });
}

TableLoader.prototype.insertData = function (items) {
    $(LoaderSettings.tableBody).empty();
    LoaderSettings.fillFunction(items);
    this.bindRowClick();
}

TableLoader.prototype.bindRowClick = function () {
    $(".clickable-row").click(function () {
        window.document.location = $(this).data("href");
    });
}

TableLoader.prototype.reloadData = function () {
    $("tbody").fadeOut(200, function () {
        $("tbody").empty();
        LoaderSettings.tblLoader.loadData();
    });
}

TableLoader.prototype.bindBtnClick = function () {
    $(".btn-group button").click(function () {
        $(".btn-group button").removeClass("active");
        $(this).addClass("active");
        LoaderSettings.btnValue = $(this).val();
        LoaderSettings.tblLoader.reloadData();
    });
}

TableLoader.prototype.searchItem = function (search) {
    if (search.length == 0) {
        if (this.doneZeroLenEvent > 0) {
            return;
        }

        $("#helpBlock2").slideUp();
        this.reloadData();
        this.doneZeroLenEvent = 1;
        return;
    }
    if (search.length < 4) {
        $("#helpBlock2").slideDown();
        return;
    } else {
        $("#helpBlock2").slideUp();
    }

    this.doneZeroLenEvent = 0;
    $("tbody").fadeOut(200, function () {
        $.ajax(LoaderSettings.ajaxSearchUrl(search, LoaderSettings.btnValue))
            .done(function (items) {
                LoaderSettings.tblLoader.insertData(items);
                $("tbody").fadeIn();
            })
            .fail(function () {

            })
    })
}

TableLoader.prototype.bindSearchEvents = function () {
    $("#itemSearch").on("change paste keyup", function () {
        LoaderSettings.tblLoader.searchItem($(this).val());
    });
}
