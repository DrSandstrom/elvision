var datePickerSettings = {
    isRTL: false,
    dateFormat: "yyyy-MM-dd",
    autoclose: true,
    language: 'sv'
};

var startTime = new Date("2016-10-03");
var finishedTime = new Date("2016-10-12");

$(document).ready(function () {
    $('#datetimepicker12').datetimepicker({
        minView: 2,
        calendarWeeks: true,
        language: 'sv',
        sideBySide: true,
        viewMode: 'days',
        format: "yyyy-mm-dd",
    });

    $("form:eq(0)").submit(function (e) {
        $("#successAlert").slideUp();
        $("#failAlert").slideUp();
        $.ajax({
            type: "POST",
            url: ajaxFormUrl,
            data: $("form:eq(0)").serialize(),
            success: function (data) {
                $("form:eq(0)")[0].reset();
                $("#successAlert").slideDown();

            },
            error: function (data) {
                var errorMsg = data.responseJSON;
                if (errorMsg[0] != "Error") {
                    $("#failAlert span").text("Formuläret innehåller felaktig data, var god kontrollera och försök igen.");
                } else {
                    $("#failAlert span").text(errorMsg[1]);
                }
                $("#failAlert").slideDown();
            }
        });
        e.preventDefault();
    });

});

