var registrationStats = {
    total: 0,
    sumHours: 0.0
};

$(document).ready(function () {
    $.each($("table:eq(1) tbody tr"), function (index, tr) {
        registrationStats.total++;
        registrationStats.sumHours += parseFloat($(tr).find("td:eq(2)").text());
    });

    $("table:eq(0) tbody tr td:eq(0)").text(registrationStats.total);
    $("table:eq(0) tbody tr td:eq(1)").text(registrationStats.sumHours);

    $("#timetable").tablesorter();

})
;