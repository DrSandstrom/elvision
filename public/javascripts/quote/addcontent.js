$(document).ready(function(){
    $("form:eq(0)").submit(function(e) {
        var url = "/quote/add/content/handle/" + tenderId;
        $("#successAlert").slideUp();
        $("#failAlert").slideUp();
        $.ajax({
            type: "POST",
            url: url,
            data: $("form:eq(0)").serialize(),
            success: function(data) {
                $("form:eq(0)")[0].reset();
                $("#successAlert").slideDown();

            },
            error: function(data) {
                $("#failAlert").slideDown();
            }
        });

        e.preventDefault();
    });
});