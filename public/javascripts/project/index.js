var LoaderSettings = {
    tableBody: "tbody",
    ajaxUrl: "/project/rest/get/all/json?type=",
    ajaxSearchUrl: function(search, type) {
        return "/project/rest/search/json?search=" + search + "&type=" + type;
    },

    btnValue: 0,

    fillFunction: function(projects) {
        jQuery.each(projects, function(index, project) {
            var row = $("<tr></tr>");
            row.attr("data-href", "/project/" + project.id + "/show").attr("class", "clickable-row");
            $(LoaderSettings.tableBody).append(row);
            row.append($("<td></td>").text(project.id));
            row.append($("<td></td>").text(project.name));
            row.append($("<td></td>").text(project.city));
            row.append($("<td></td>").text(project.address));
            row.append($("<td></td>").text(new Date(project.startTime).toLocaleString()));
            row.append($("<td></td>").text(new Date(project.finishedTime).toLocaleString()));
            row.append($("<td></td>").text((project.customer.customerType == 0 ? "Privat" : "Företag")));
        });
    },
    tblLoader: null
};

$(document).ready(function() {
    $("table").show();
    var tableLoader = new TableLoader();
});