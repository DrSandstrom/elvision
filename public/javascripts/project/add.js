var datePickerSettings = {
    isRTL: false,
    /*format: 'yyyy-mm-dd hh:ii:ss',
    dateFormat: "yyyy-mm-dd hh:ii:ss",
    */
    autoclose: true,
    language: 'sv'
};

$(document).ready(function () {
    $("#startDate").datetimepicker(datePickerSettings);
    $("#stopDate").datetimepicker(datePickerSettings);


});