var LoaderSettings = {
    tableBody: "tbody",
    ajaxUrl: "/employee/rest/get/all/json",
    ajaxSearchUrl: function (search) {
        return "/employee/rest/search/json?search=" + search;
    },

    btnValue: "",

    fillFunction: function (employees) {
        jQuery.each(employees, function (index, employee) {
            var row = $("<tr></tr>");
            row.attr("data-href", "/employee/" + employee.id + "/show").attr("class", "clickable-row");
            $("tbody").append(row);

            LoaderSettings.fillEmployee(row, employee);
        });
    },

    fillEmployee: function (element, employee) {
        element.append($("<td></td>").text(employee.groupName));
        element.append($("<td></td>").text(employee.firstname));
        element.append($("<td></td>").text(employee.lastname));
        element.append($("<td></td>").text(employee.email));
    },

    tblLoader: null
};

$(document).ready(function () {
    $("table").show();
    var tableLoader = new TableLoader();
});

