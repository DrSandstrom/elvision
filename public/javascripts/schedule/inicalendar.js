
$(document).ready(function () {

    // page is now ready, initialize the calendar...

    $('#calendar').fullCalendar({
        // put your options and callbacks here
        locale: 'sv',
        contentHeight: 500,
        eventColor: '#07719e',
        noEventsMessage: "Det finns inget inlagd på det angivna datumet.",

        header: {
            left: 'prev,next today,listWeek',
            center: false,
            right: false,
        },

        buttonText: {
            today: 'Idag'
        },

        // customize the button names,
        // otherwise they'd all just say "list"
        views: {
            listWeek: {buttonText: 'Vecka'}
        },

        defaultView: 'listWeek',
        navLinks: true, // can click day/week names to navigate views
        editable: false,

        events: [
            {
                title: 'Bygga ut strandvägen',
                start: '2016-10-17T07:20',
                end: '2016-10-17T12:10'
            },
            {
                title: 'Träff med Leif',
                start: '2016-10-17T10:30',
                end: '2016-10-17T12:30'
            },
            {
                title: 'Lunch',
                start: '2016-10-17T12:20',
                end: '2016-10-17T13:40'
            },
            {
                title: 'Träff med Per',
                start: '2016-10-18T10:40',
                end: '2016-10-18T12:10'
            },
            {
                title: 'Lunch',
                start: '2016-10-18T12:20',
                end: '2016-10-18T13:40'
            },
            {
                title: 'Möte med företag',
                start: '2016-10-19T09:20',
                end: '2016-10-19T12:10'
            },
            {
                title: 'Lunch',
                start: '2016-10-19T12:20',
                end: '2016-10-19T13:40'
            },
        ],
        timeFormat: 'H(:mm)'
    });
});