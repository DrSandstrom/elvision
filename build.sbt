name := "play-java"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.typesafe.play" % "play-ebean_2.11" % "3.0.2",
  "mysql" % "mysql-connector-java" % "5.1.39",
  javaJdbc,
  cache,
  javaWs
)
