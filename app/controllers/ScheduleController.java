package controllers;

import models.*;
import views.html.*;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.Authentication;
import security.authentication.GroupAuth;
import security.authentication.IsLoggedIn;


import javax.inject.Inject;

@IsLoggedIn
public class ScheduleController extends Controller {

    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    public Result index() {
        return ok(scheduleIndex.render(authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result add() {
        return Results.TODO;
    }

    @GroupAuth("admin")
    public Result handleAdd() {
        return Results.TODO;
    }

    @GroupAuth("admin")
    public Result edit(long id) {
        return Results.TODO;
    }

    @GroupAuth("admin")
    public Result handleEdit(long id) {
        return Results.TODO;
    }

    @GroupAuth("admin")
    public Result delete(long id) {
        return Results.TODO;
    }

}
