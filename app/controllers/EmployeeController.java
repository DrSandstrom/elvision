package controllers;

import models.Employee;
import models.Group;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import security.Authentication;
import security.Password;
import security.authentication.GroupAuth;
import security.authentication.IsLoggedIn;
import views.html.*;

import javax.inject.Inject;
import java.util.List;

import static play.libs.Json.toJson;

@IsLoggedIn
public class EmployeeController extends Controller {

    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    @Inject
    private FormFactory formFactory;

    public Result index() {
        return ok(employeeIndex.render(authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result add() {
        return ok(employeeAdd.render(authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result handleAdd() {
        Employee employee = formFactory.form(Employee.class).bindFromRequest().get();

        String[] params = request().body().asFormUrlEncoded().get("passhash");

        if (params == null || params[0] == null)
            return Results.badRequest();

        employee.setPasshash(Password.create(params[0]));

        employee.save();
        employee.update();
        return Results.redirect(routes.HomeController.index());
    }

    public Result show(int id) {
        Employee targetEmployee = Employee.getEmployeeById(id);
        if (targetEmployee == null)
            return ErrorController.notFound("Det finns ingen anställd med id #" + id);

        return ok(employeeShow.render(authentication.getEmployeeBySession(), group, targetEmployee));
    }

    public Result jsonSearch(String search) {
        List<Employee> employees;

        if (search == null || search.isEmpty() || search.length() < 4) {
            return Results.badRequest();
        }
        employees = Employee.finder.where().ilike("email", "%" + search + "%").findList();
        return ok(toJson(employees));
    }

    public Result jsonGetAll() {

        return ok(toJson(Employee.finder.all()));
    }

    @GroupAuth("admin")
    public Result edit(long id) {
        Employee employee = authentication.getEmployeeBySession();
        Employee targetEmployee = Employee.getEmployeeById((int)id);
        if (targetEmployee == null)
            return ErrorController.notFound("Det finns ingen anställd med id #" + id);

        if (!employeeCanEditOther(employee, targetEmployee))
            return Results.redirect(routes.ErrorController.unAuth());

        return ok(employeeEdit.render(employee, group, targetEmployee));
    }

    @GroupAuth("admin")
    public Result handleEdit(long id) {
        Employee targetEmployee = Employee.getEmployeeById((int)id);
        if (targetEmployee == null)
            return Results.badRequest();

        DynamicForm data = formFactory.form().bindFromRequest();
        // Automatically gets the radio button which is selected.
        Form.Field groupField = data.field("groupName");
        if (groupField != null && paramIsUsable(groupField.value())) {
            targetEmployee.setGroupName(groupField.value());
        }
        String firstname = data.get("firstname");
        if (paramIsUsable(firstname)) {
            targetEmployee.setFirstname(firstname);
        }
        String lastname = data.get("lastname");
        if (paramIsUsable(lastname)) {
            targetEmployee.setLastname(lastname);
        }
        String email = data.get("email");
        if (paramIsUsable(email)) {
            targetEmployee.setEmail(email);
        }
        String password = getFirstParam(request(), "passhash");
        if (paramIsUsable(password)) {
            targetEmployee.setPasshash(Password.create(password));
        }

        targetEmployee.update();
        return Results.redirect(routes.HomeController.index());
    }

    @GroupAuth("admin")
    public Result delete(long id) {
        Employee employee = authentication.getEmployeeBySession();
        Employee targetEmployee = Employee.getEmployeeById((int)id);
        if (targetEmployee == null)
            return ErrorController.notFound("Det finns ingen anställd med id #" + id);

        if (!employeeCanEditOther(employee, targetEmployee))
            return Results.redirect(routes.ErrorController.unAuth());

        return ok(employeeDelete.render(employee, group, targetEmployee));
    }

    @GroupAuth("admin")
    public Result handleDelete(long id) {
        Employee targetEmployee = Employee.getEmployeeById((int)id);
        if (targetEmployee == null)
            return Results.badRequest();

        targetEmployee.delete();
        return Results.redirect(routes.HomeController.index());
    }

    private String getFirstParam(Http.Request request, String paramName) {
        String[] allParams = request.body().asFormUrlEncoded().get(paramName);
        if (allParams == null) return null;
        return allParams[0];
    }

    private boolean paramIsUsable(String param) {
        return ((param != null) && !param.trim().isEmpty());
    }

    private boolean employeeCanEditOther(Employee editor, Employee other) {
        // Can always edit yourself.
        if (editor.getId() == other.getId()) return true;
        int userAccessLevel = group.getAccessLevel(editor.getGroupName());
        int requiredAccessLevel = group.getAccessLevel(other.getGroupName());
        return (userAccessLevel > requiredAccessLevel);
    }
}
