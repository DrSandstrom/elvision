package controllers;

import models.Employee;
import play.Logger;
import play.api.cache.CacheApi;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import security.Authentication;
import security.Password;
import views.html.*;

import javax.inject.Inject;

public class LoginController extends Controller {

    @Inject
    private FormFactory formFactory;

    @Inject
    private Authentication authentication;

    public static class LoginParameters {

        public String email;
        public String password;
    }

    public Result login() {
        if (authentication.isLoggedIn()) {
            return redirect(routes.HomeController.index());
        }

        return ok(login.render());
    }

    public Result takeLogin() {
        LoginParameters loginParams = formFactory.form(LoginParameters.class).bindFromRequest().get();

        Employee employee = Employee.finder.where().eq("email", loginParams.email).findUnique();

        if (employee == null) {
            Logger.debug("LoginController failed due to the object being empty, ie. null");
            return redirect(controllers.routes.LoginController.loginFail());
        } else if (Password.compare(employee, loginParams.password)) {
            Logger.debug("LoginController was successful.");
            authentication.setSession(employee);
            return redirect(routes.HomeController.index());
        }

        Logger.debug("LoginController failed due to bad password.");
        return redirect(controllers.routes.LoginController.loginFail());
    }

    public Result loginFail() {
        return ok("Inloggningen misslyckades.");
    }

    public Result logOut() {
        authentication.destroySession();
        return redirect(controllers.routes.LoginController.login());
    }

    public Result loginTest() {
        if (!authentication.isLoggedIn()) {
            return ok("Du är ju för fan inte inloggad!");
        } else {
            return ok("Du är ju för fan inloggad!");
        }
    }

}
