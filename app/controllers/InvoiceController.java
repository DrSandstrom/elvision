package controllers;

import models.Invoice;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.List;

import static play.libs.Json.toJson;

public class InvoiceController extends Controller {
    @Inject
    private FormFactory formFactory;

    public Result index() {
        List<Invoice> customers = Invoice.finder.all();
        return ok(toJson(customers));
    }

    public Result add() {
        Invoice invoice = formFactory.form(Invoice.class).bindFromRequest().get();
        return ok(toJson(invoice));
    }

    public Result handleAdd() {
        return Results.TODO;
    }

    public Result show(long id) {
        return Results.TODO;
    }

    public Result edit(long id) {
        return Results.TODO;
    }

    public Result handleEdit(long id) {
        return Results.TODO;
    }

    public Result delete(long id) {
        return Results.TODO;
    }
}
