package controllers;


import models.Group;
import play.*;
import play.api.OptionalSourceMapper;
import play.api.UsefulException;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.mvc.Http.*;
import play.mvc.*;
import security.Authentication;

import javax.inject.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Results.ok;
import static play.mvc.Results.redirect;

import views.html.*;

@Singleton
public class ErrorController extends DefaultHttpErrorHandler {
    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    @Inject
    public ErrorController(Configuration configuration, Environment environment,
                           OptionalSourceMapper sourceMapper, Provider<Router> routes) {
        super(configuration, environment, sourceMapper, routes);
    }


    protected CompletionStage<Result> onUnauthorized(Http.RequestHeader request, String message) {
        if (authentication.isLoggedIn()) {
            return CompletableFuture.completedFuture(
                    unAuth()
            );
        }

        return CompletableFuture.completedFuture(
                redirect(routes.LoginController.login())
        );
    }

    protected CompletionStage<Result> onNotFound(Http.RequestHeader request, String message) {
        return CompletableFuture.completedFuture(
            notFound("Sidan du sökte hittades inte, kontrollera sökvägen och försök igen.")
        );
    }

    public Result unAuth() {
        return ok(unauthorizedError.render(authentication.getEmployeeBySession(), group));
    }

    public static Result notFound(String message) {
        return Results.notFound(notFoundError.render(message));
    }

}
