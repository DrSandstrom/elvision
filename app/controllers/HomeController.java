package controllers;

import models.Employee;
import models.Group;
import play.Logger;
import play.mvc.*;

import security.Authentication;
import security.Password;
import security.authentication.IsLoggedIn;
import views.html.*;

import javax.inject.Inject;
import java.util.List;

import static play.libs.Json.toJson;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {
    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    public Result createUser() {
        Employee empl = new Employee();
        empl.setEmail("kalle@gurra.se");
        empl.setFirstname("Kalle");
        empl.setLastname("Karlsson");
        empl.setGroupName("root");
        empl.setPasshash(Password.create("asdf"));
        empl.save();

        Logger.debug("testuser created.");
        Logger.debug("Email: " + empl.getEmail() + ", Password: " + "asdf");

        return ok("FIXAT JAO");
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    @IsLoggedIn
    public Result index() {
        return ok(index.render(authentication.getEmployeeBySession(), group));
    }


}
