package controllers;

import models.*;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.Authentication;
import security.authentication.GroupAuth;
import security.authentication.IsLoggedIn;
import views.html.*;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static play.libs.Json.toJson;

@IsLoggedIn
public class CustomerController extends Controller {
    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    @Inject
    private FormFactory formFactory;

    public Result jsonGetAll(int type) {
        List<Customer> customers = null;

        if (type == 0) {
            customers = Customer.finder.all();
        } else if (type >= 1 && type <= 2) {
            customers = Customer.finder.where().eq("customer_type", --type).findList();
        }

        return ok(toJson(customers));
    }

    @GroupAuth("admin")
    public Result handleAdd() {
        Customer customer = formFactory.form(Customer.class).bindFromRequest().get();

        if (customer.getCustomerType() == 0) {
            customer.setPrivateCustomer(formFactory.form(PrivateCustomer.class).bindFromRequest().get());
            customer.getPrivateCustomer().save();
        } else if (customer.getCustomerType() == 1) {
            customer.setCorporateCustomer(formFactory.form(CorporateCustomer.class).bindFromRequest().get());
            customer.getCorporateCustomer().save();
        } else {
            return Results.badRequest("Bad request dawg");
        }

        customer.save();
        customer.update();
        return redirect(controllers.routes.CustomerController.show(customer.getId()));
    }

    public Result show(int id) {
        Customer customer = Customer.finder.byId(id);

        if (customer == null) {
            return Results.notFound("404");
        }

        return ok(customerShow.render(customer, authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result edit(long id) {
        Customer customer = Customer.finder.byId((int)id);
        if (customer == null)
            return ErrorController.notFound("Det finns ingen kund med id #" + id);

        return ok(customerEdit.render(customer, authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result handleEdit(long id) {
        Customer customer = Customer.finder.byId((int)id);
        Customer customerTemplate = formFactory.form(Customer.class).bindFromRequest().get();
        if (customer == null || customerTemplate == null)
            return Results.badRequest();

        customer.setPhoneNumber(customerTemplate.getPhoneNumber());
        customer.setCellPhoneNumber(customerTemplate.getCellPhoneNumber());
        customer.setEmail(customerTemplate.getEmail());
        customer.setAddress(customerTemplate.getAddress());
        customer.setZipCode(customerTemplate.getZipCode());

        if (customer.getCustomerType() == 0) {
            PrivateCustomer privateCustomerTemplate = formFactory.form(PrivateCustomer.class).bindFromRequest().get();
            privateCustomerTemplate.setId(customer.getPrivateCustomer().getId());
            customer.setPrivateCustomer(privateCustomerTemplate);
            customer.getPrivateCustomer().update();
        } else if (customer.getCustomerType() == 1) {
            CorporateCustomer corporateCustomerTemplate = formFactory.form(CorporateCustomer.class).bindFromRequest().get();
            corporateCustomerTemplate.setId(customer.getCorporateCustomer().getId());
            customer.setCorporateCustomer(corporateCustomerTemplate);
            customer.getCorporateCustomer().update();
        } else {
            return Results.badRequest("Bad request dawg");
        }

        customer.update();

        return Results.redirect(controllers.routes.CustomerController.show(customer.getId()));
    }

    @GroupAuth("admin")
    public Result delete(long id) {
        Customer customer = Customer.finder.byId((int)id);
        if (customer == null)
            return ErrorController.notFound("Det finns ingen kund med id #" + id);

        return ok(customerDelete.render(customer, authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result handleDelete(long id) {
        Customer customer = Customer.finder.byId((int)id);
        if (customer == null)
            return Results.badRequest();

        customer.delete();

        if (customer.getCustomerType() == 0) {
            customer.getPrivateCustomer().delete();
        } else if (customer.getCustomerType() == 1) {
            customer.getCorporateCustomer().delete();
        } else {
            return Results.badRequest("Hur kom du ens hit? Kunder läggs inte till korrekt");
        }

        return Results.redirect(routes.HomeController.index());
    }

    /* Displays the customer summary page */
    public Result index() {
        return ok(customerIndex.render(authentication.getEmployeeBySession(), group));
    }

    @GroupAuth("admin")
    public Result add() {
        return ok(customerAdd.render(authentication.getEmployeeBySession(), group));
    }

    public Result jsonSearch(String search, int customerType) {
        List<Customer> privateCustomers = null;
        List<Customer> corporateCustomers = null;
        List<Customer> customers = null;

        if (search == null || search.isEmpty() || search.length() < 4) {
            return Results.badRequest();
        }

        if (customerType == 0 || customerType == 1) {
            privateCustomers = Customer.finder.where().eq("customer_type", 0)
                    .and()
                    .ilike("concat(privateCustomer.firstname, ' ', privateCustomer.lastname)", "%" + search + "%")
                    .findList();
        }

        if (customerType == 0 || customerType == 2) {
            corporateCustomers = Customer.finder.where().eq("customer_type", 1)
                    .and()
                    .ilike("corporateCustomer.companyName", "%" + search + "%")
                    .findList();
        }

        if (customerType == 0) {
            customers = Stream.concat(privateCustomers.stream(), corporateCustomers.stream()).collect(Collectors.toList());
        } else if (customerType == 1) {
            customers = privateCustomers;
        } else if (customerType == 2) {
            customers = corporateCustomers;
        }

        return ok(toJson(customers));
    }


}
