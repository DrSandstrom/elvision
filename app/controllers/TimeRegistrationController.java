package controllers;

import models.Group;
import models.Project;
import models.TimeRegistration;
import org.joda.time.DateTime;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Result;
import play.mvc.Results;
import security.Authentication;
import security.authentication.IsLoggedIn;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import views.html.*;

import static play.mvc.Controller.request;
import static play.mvc.Results.ok;
import static util.Error.jsonError;

@IsLoggedIn
public class TimeRegistrationController {
    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    @Inject
    private FormFactory formFactory;

    public Result add(int projectId) {
        Project project = Project.finder.byId(projectId);

        if (project == null) {
            return Results.notFound("404 - Project not found");
        }

        return ok(timeRegistrationAdd.render(authentication.getEmployeeBySession(), group, project));
    }

    public Result handleAdd(int projectId) {
        Form<TimeRegistration> form = formFactory.form(TimeRegistration.class).bindFromRequest();

        if (form.hasErrors()) {
            return Results.badRequest(form.errorsAsJson());
        }

        TimeRegistration timeRegistration = form.get();

        Project project = Project.finder.byId(projectId);
        if (project == null) {
            return Results.notFound(jsonError("Projektet existerar inte"));
        }

        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMAN);
            Date date = df.parse(request().body().asFormUrlEncoded().get("date")[0]);
            timeRegistration.setDate(date);

            if (date.before(project.getStartTime()) || date.after(project.getFinishedTime())) {
                /**
                 * Since the submitted date from timeRegistrationAdd form
                 * is in the yyyy-MM-dd format, when DateFormat parses it, it assumes
                 * the date is yyyy-MM-dd 00:00:00, therefore any checks done when
                 * the submitted date is the same day as either project.startTime or project.finishedTime
                 * will return false.
                 * To circumvent this we do additional checks to see if it really is wrong.
                 */
                DateTime startTime = new DateTime(project.getStartTime());
                DateTime finishedTime = new DateTime(project.getFinishedTime());

                /* It's okay to just look away. :) */
                Date dstart = df.parse(startTime.getYear() + "-" + startTime.getMonthOfYear() + "-" + startTime.dayOfMonth().get());
                Date dfinish = df.parse(finishedTime.getYear() + "-" + finishedTime.getMonthOfYear() + "-" + finishedTime.dayOfMonth().get());

                if (date.before(dstart) || date.after(dfinish)) {
                    return Results.badRequest(jsonError("Datumet är utanför projektets tidsram."));
                }

            }
        } catch (Exception e) {
            return Results.badRequest(jsonError("Datumet är i fel format, kontrollera att formatet är exempelvis: 2016-04-03"));
        }


        timeRegistration.setEmployee(authentication.getEmployeeBySession());
        timeRegistration.setProject(project);
        timeRegistration.save();

        return ok();
    }

    public Result show(int projectId) {
        Project project = Project.finder.byId(projectId);

        if (project == null) {
            return Results.notFound("Project not found");
        }

        List<TimeRegistration> timeRegistrations = TimeRegistration.getTimeRegistrationsByProject(projectId);

        if (timeRegistrations.isEmpty()) {
            return ok("Det finns inga.");
        }

        return ok(timeRegistrationShow.render(authentication.getEmployeeBySession(), group, project, timeRegistrations));
    }
}
