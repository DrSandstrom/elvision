package controllers;

import models.Customer;
import models.Group;
import models.Quote;
import models.QuoteContent;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.Authentication;
import security.authentication.GroupAuth;
import security.authentication.IsLoggedIn;
import views.html.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by hannes on 2016-10-04.
 */
@IsLoggedIn
@GroupAuth("admin")
public class QuoteController extends Controller {
    @Inject
    private Authentication authentication;

    @Inject
    private Group group;

    @Inject
    private FormFactory formFactory;

    public Result handleContentAdd(int quoteId) {
        Quote quote = Quote.finder.byId(quoteId);

        if (quote == null) {
            return Results.notFound();
        }

        QuoteContent quoteContent = formFactory.form(QuoteContent.class).bindFromRequest().get();
        quoteContent.setQuote(quote);
        quoteContent.save();

        return ok();
    }

    public Result add(int customerId) {
        Customer customer = Customer.finder.byId(customerId);

        if (customer == null) {
            return Results.notFound(notFoundError.render("Kunden existerar inte."));
        }


        return ok(quoteAdd.render(authentication.getEmployeeBySession(), group, customer));
    }

    public Result handleAdd(int customerId) {
        Customer customer = Customer.finder.byId(customerId);

        if (customer == null) {
            return Results.notFound(notFoundError.render("Kunden existerar inte."));
        }

        Quote quote = formFactory.form(Quote.class).bindFromRequest().get();
        quote.setCustomer(customer);
        quote.save();
        quote.update();

        return redirect(controllers.routes.QuoteController.show(quote.getId()));
    }

    public Result show(int quoteId) {
        Quote quote = Quote.finder.byId(quoteId);

        if (quote == null) {
            return Results.notFound(notFoundError.render("Offerten existerar inte."));
        }

        return ok(quoteShow.render(authentication.getEmployeeBySession(), group, quote));
    }

    public Result addContent(int quoteId) {
        Quote quote = Quote.finder.byId(quoteId);

        if (quote == null) {
            return Results.notFound(notFoundError.render("Offerten existerar inte."));
        }

        return ok(quoteAddContent.render(authentication.getEmployeeBySession(), group, quote));
    }

    public Result index(int customerId) {
        Customer customer = Customer.finder.byId(customerId);

        if (customer == null) {
            return Results.notFound(notFoundError.render("Kunden existerar inte."));
        }

        List<Quote> quotes = customer.getQuotes();

        if (quotes.size() == 0) {
            return Results.notFound(notFoundError.render("Kunden har inga offerter att visa."));
        }

        return ok(quoteIndex.render(authentication.getEmployeeBySession(), group, customer, quotes));
    }

}
