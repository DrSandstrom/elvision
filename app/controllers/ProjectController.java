package controllers;


import models.Customer;
import models.Employee;
import models.Group;
import models.Project;
import play.data.Form;
import play.mvc.*;
import play.data.FormFactory;
import security.Authentication;
import security.authentication.GroupAuth;
import security.authentication.IsLoggedIn;
import views.html.*;

import javax.inject.Inject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static play.libs.Json.toJson;


@IsLoggedIn
public class ProjectController extends Controller {
    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    @Inject
    private FormFactory formFactory;

    @GroupAuth("admin")
    public Result add(int customerId) {
        Customer customer = Customer.getById(customerId);

        if (customer == null) {
            return ok("sweden");
        }

        return ok(projectAdd.render(authentication.getEmployeeBySession(), group, customer));
    }

    public Result handleAdd() {
        Form<Project> form = formFactory.form(Project.class).bindFromRequest();
        Project project = form.get();

        Customer customer = Customer.getById(Integer.parseInt(request().body().asFormUrlEncoded().get("customerId")[0]));
        project.setCustomer(customer);

        /**
         * Play doesn't like our supplied datetimes.
         * So we add them manually.
         */
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	    
            Date finishedTime = df.parse(request().body().asFormUrlEncoded().get("finishedTime")[0].trim());
            Date startTime = df.parse(request().body().asFormUrlEncoded().get("startTime")[0].trim());
            project.setStartTime(startTime);
            project.setFinishedTime(finishedTime);

            if (startTime.after(finishedTime)) {
                return Results.badRequest("Server made a boo boo");
            }

        } catch (ParseException e) {
    		return Results.internalServerError("Server made a boo boo: " + e.getMessage());
        }

        if (form.hasErrors()) {
            return ok(form.errorsAsJson());
        }

        project.save();
        return redirect(controllers.routes.ProjectController.show(project.getId()));
    }

    public Result show(int id) {
        Project project = Project.finder.byId(id);

        if (project == null) {
            return ErrorController.notFound("Projektet du söker existerar inte.");
        }

        return ok(projectShow.render(project, authentication.getEmployeeBySession(), group));
    }

    public Result jsonGetAll(int type) {
        List<Project> projects = null;
        if (type > 0) {
            projects = Project.finder.where().eq("customer.customerType", --type).findList();
        } else {
            projects = Project.finder.all();
        }

        return ok(toJson(projects));
    }

    public Result jsonSearch(String search, int customerType) {
        List<Project> privateProjects = null;
        List<Project> corporateProjects = null;
        List<Project> projects = null;

        if (customerType == 0 || customerType == 1) {
            privateProjects = Project.finder.where().eq("customer.customerType", 0)
                    .ilike("name", "%" + search + "%").findList();
        }

        if (customerType == 0 || customerType == 2) {
            corporateProjects = Project.finder.where().eq("customer.customerType", 1)
                    .ilike("name", "%" + search + "%").findList();
        }

        if (customerType == 0) {
            projects = Stream.concat(privateProjects.stream(), corporateProjects.stream()).collect(Collectors.toList());
        } else if (customerType == 1) {
            projects = privateProjects;
        } else if (customerType == 2) {
            projects = corporateProjects;
        }


        return ok(toJson(projects));
    }

    public Result index() {
        return ok(projectIndex.render(authentication.getEmployeeBySession(), group));
    }

    public Result edit(long id) {
        return Results.TODO;
    }

    public Result delete(long id) {
        return Results.TODO;
    }

}
