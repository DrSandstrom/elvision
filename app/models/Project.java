package models;

import com.avaje.ebean.Model;
import play.data.format.Formats;
import play.data.format.Formats.DateFormatter;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.Date;
import java.util.List;

@Entity
public class Project extends Model {
    public static Model.Finder<Integer, Project> finder = new Model.Finder<>(Project.class);

    @Id
    private int id;

    private Date startTime;

    private Date finishedTime;

    @Column(columnDefinition = "boolean default false")
    private boolean confirmed;

    private Employee confirmedBy;

    @Constraints.Required
    private int zipCode;

    @Constraints.Required
    @Constraints.MaxLength(30)
    @Constraints.MinLength(10)
    private String name;

    @Constraints.Required
    @Constraints.MaxLength(40)
    @Constraints.MinLength(7)
    private String address;

    @Constraints.Required
    @Column(columnDefinition = "TEXT")
    private String description;

    @Constraints.Required
    private String city;

    @OneToMany(mappedBy = "project")
    private List<Schedule> schedules;

    @OneToMany(mappedBy = "project")
    private List<Invoice> invoices;

    @ManyToOne
    private Customer customer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(Date finishedTime) {
        this.finishedTime = finishedTime;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
