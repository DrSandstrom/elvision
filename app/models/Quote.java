package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.Logger;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class Quote extends Model {
    public static Model.Finder<Integer, Quote> finder = new Model.Finder<>(Quote.class);

    @Id
    private int id;

    @Constraints.Required
    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    @OneToMany
    @JsonManagedReference
    private List<QuoteContent> quoteContents;

    @ManyToOne
    @JsonBackReference
    private Customer customer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QuoteContent> getQuoteContents() {
        return quoteContents;
    }

    public void setQuoteContents(List<QuoteContent> quoteContents) {
        this.quoteContents = quoteContents;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getTotalCostByCategory(int category) {
        BigDecimal totalCost = new BigDecimal("0.00");

        for (QuoteContent quoteContent : getQuoteContents()) {
            if (quoteContent.getCategory() == category) {
                Logger.debug("HITTADE NICE!");
                totalCost = totalCost.add(quoteContent.getTotalCost());
            }
        }

        Logger.debug("TOTALA KOSTNADEN: " + totalCost.toPlainString());

        return totalCost;
    }

    public BigDecimal getTotalCost() {
        BigDecimal totalCost = new BigDecimal("0.00");

        for (QuoteContent quoteContent : getQuoteContents()) {
            totalCost = totalCost.add(quoteContent.getTotalCost());
        }

        return totalCost;
    }

}
