package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;


@Entity
public class TimeRegistration extends Model {
    public static Model.Finder<Integer, TimeRegistration> finder = new Model.Finder<>(TimeRegistration.class);

    @Id
    private int id;

    @Constraints.Required
    private double timeWorked;

    private Date date;

    @Column(columnDefinition = "TEXT")
    @Constraints.Required
    private String workDescription;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Employee employee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTimeWorked() {
        return timeWorked;
    }

    public void setTimeWorked(double timeWorked) {
        this.timeWorked = timeWorked;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public static List<TimeRegistration> getTimeRegistrationsByProject(int projectId) {
        return finder.where().eq("project.id", projectId).findList();
    }

}
