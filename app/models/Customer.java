package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * TODO: Add validation of class members.
 */
@Entity
public class Customer extends Model {
    public static Model.Finder<Integer, Customer> finder = new Model.Finder<>(Customer.class);

    @Id
    private int id;

    @Constraints.Required
    private String phoneNumber;

    @Constraints.Required
    private String cellPhoneNumber;

    @Column(unique = true)
    @Constraints.Email
    @Constraints.Required
    private String email;

    @Constraints.Required
    private String address;

    @Constraints.Required
    private int zipCode;

    @Constraints.Required
    @Constraints.Min(0)
    @Constraints.Max(1)
    private int customerType;

    @OneToOne
    private PrivateCustomer privateCustomer;

    @OneToOne
    private CorporateCustomer corporateCustomer;

    @OneToMany
    @JsonManagedReference
    private List<Quote> quotes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public PrivateCustomer getPrivateCustomer() {
        return privateCustomer;
    }

    public void setPrivateCustomer(PrivateCustomer privateCustomer) {
        this.privateCustomer = privateCustomer;
    }

    public CorporateCustomer getCorporateCustomer() {
        return corporateCustomer;
    }

    public void setCorporateCustomer(CorporateCustomer corporateCustomer) {
        this.corporateCustomer = corporateCustomer;
    }

    public int getCustomerType() {
        return customerType;
    }

    public void setCustomerType(int customerType) {
        this.customerType = customerType;
    }

    public static Customer getById(int id) {
        return finder.byId(id);
    }

    public String getCustomerName() {
        if (customerType == 1) {
            return corporateCustomer.getCompanyName();
        } else {
            return new String(privateCustomer.getFirstname() + " " + privateCustomer.getLastname());
        }
    }


    public List<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<Quote> quotes) {
        this.quotes = quotes;
    }
}
