package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import security.Authentication;

import javax.persistence.*;
import java.util.List;

import static play.mvc.Controller.session;

@Entity
public class Employee extends Model {
    public static Model.Finder<Integer, Employee> finder = new Model.Finder<>(Employee.class);

    @Transient
    public byte[] sessionHash;

    @Id
    private int id;

    @Constraints.Required
    private String groupName;

    @JsonIgnore
    @Column(length = 32)
    private byte[] passhash;

    @Constraints.Required
    private String firstname;

    @Constraints.Required
    private String lastname;

    @Column(unique = true)
    @Constraints.Email
    @Constraints.Required
    private String email;

    @OneToMany(mappedBy = "employee")
    private List<Schedule> schedule;

    public static Finder<Integer, Employee> getFinder() {
        return finder;
    }

    public static void setFinder(Finder<Integer, Employee> finder) {
        Employee.finder = finder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getPasshash() {
        return passhash;
    }

    public void setPasshash(byte[] passhash) {
        this.passhash = passhash;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public static Employee getEmployeeById(int id) {
        return finder.byId(id);
    }

    public static Employee getEmployeeByEmail(String email) {
        return finder.where().eq("email", email).findUnique();
    }

    public byte[] getSessionHash() {
        return sessionHash;
    }

    public void setSessionHash(byte[] sessionHash) {
        this.sessionHash = sessionHash;
    }
}
