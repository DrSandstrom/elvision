package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
public class QuoteContent extends Model {
    @Id
    private int id;

    @Constraints.Required
    private String name;

    @Constraints.Required
    @Constraints.Min(1)
    private int amount;

    @Constraints.Required
    private String unit;

    @Column(precision = 38, scale = 2)
    @Constraints.Required
    @Constraints.Min(1)
    private BigDecimal cost;

    @Column(columnDefinition = "TEXT")
    @Constraints.Required
    private String description;

    // 0 == Arbete
    // 1 == Materialkostnad
    // 2 == Resekostnader
    // 3 == Övrigt
    @Constraints.Required
    @Constraints.Min(0)
    @Constraints.Max(3)
    private int category;

    @ManyToOne
    @JsonBackReference
    private Quote quote;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public Quote getQuote() {
        return quote;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public BigDecimal getTotalCost() {
        return cost.multiply(BigDecimal.valueOf(getAmount()));
    }
}
