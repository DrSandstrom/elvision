package models;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class Group {

    private static class GroupNameVal {
        private int accessLevel;
        private String name;

        public GroupNameVal(int accessLevel, String name) {
            this.accessLevel = accessLevel;
            this.name = name;
        }

        public int getAccessLevel() {
            return accessLevel;
        }

        public String getName() {
            return name;
        }
    }

    private Map<String, GroupNameVal> groupList;

    public Group() {
        groupList = new HashMap<>();
        groupList.put("employee", new GroupNameVal(0, "Anställd"));
        groupList.put("admin", new GroupNameVal(1, "Administratör"));
        groupList.put("root", new GroupNameVal(2, "Ägare"));
    }

    public int getAccessLevel(String groupName) {
        GroupNameVal grpNameVal = groupList.get(groupName);

        if (grpNameVal == null) {
            return -1;
        }

        return grpNameVal.getAccessLevel();
    }

    public String getFullGroupName(String groupName) {
        GroupNameVal grpNameVal = groupList.get(groupName);

        if (grpNameVal == null) {
            return null;
        }

        return grpNameVal.getName();
    }

}
