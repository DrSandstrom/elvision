package util;

import com.fasterxml.jackson.databind.JsonNode;

import static play.libs.Json.toJson;

public class Error {
    public static JsonNode jsonError(String message) {
        String error[] = new String[]{"Error", message};
        return toJson(error);
    }
}
