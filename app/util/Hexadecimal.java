package util;

public class Hexadecimal {

    private static final byte[] hexTbl = {
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    public static byte[] encode(byte[] raw) {
        byte[] hex = new byte[raw.length * 2];
        int c;

        for (int hexIt = 0, rawIt = 0; rawIt < raw.length; rawIt++) {
            c = raw[rawIt] & 0xFF;
            hex[hexIt++] = hexTbl[c >> 4];
            hex[hexIt++] = hexTbl[c & 0xF];
        }

        return hex;
    }

    public static String encode(String s) {
        return new String(encode(s.getBytes()));
    }

    public static byte[] encodeStringToBytes(String s) {
        return encode(s.getBytes());
    }

    public static String encodeBytesToString(byte[] raw) {
        return new String(encode(raw));
    }

    public static byte[] decode(byte[] hex) {
        byte[] raw = new byte[hex.length / 2];
        short b1, b2;

        for (int hexIt = 0, rawIt = 0; rawIt < raw.length; rawIt++) {
            b1 = intVal(hex[hexIt++]);
            b2 = intVal(hex[hexIt++]);

            raw[rawIt] = (byte) ((b1 << 4) | b2);
        }

        return raw;
    }


    public static String decode(String s) {
        return new String(decode(s.getBytes()));
    }

    public static byte[] decodeStringToBytes(String s) {
        return decode(s.getBytes());
    }

    public static String decodeBytesToString(byte[] hex) {
        return new String(decode(hex));
    }

    public static byte decode(byte c1, byte c2) {
        return ((byte) (intVal(c1) << 4 | intVal(c2)));
    }

    public static short intVal(byte c) {
        if ('0' <= c && c <= '9') {
            return (short) (c - '0');
        } else if ('a' <= c && c <= 'f') {
            return (short) (c - 'a' + 10);
        } else if ('A' <= c && c <= 'F') {
            return (short) (c - 'A' + 10);
        } else {
            return (short) Character.MAX_VALUE;
        }
    }
}
