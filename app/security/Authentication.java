package security;

import models.Employee;
import play.Configuration;
import play.Logger;
import play.cache.CacheApi;
import util.Hexadecimal;

import javax.inject.Inject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import static play.mvc.Controller.request;
import static play.mvc.Controller.session;

public class Authentication {
    private CacheApi cache;

    @Inject
    private Configuration configuration;

    private final static String sessionNamespace = "Sessions.";
    private final static String sessionName = "SESSID";
    private final static int sessionLifeSpan = 60 * 30;

    @Inject
    public Authentication(CacheApi cache) {
        this.cache = cache;
    }

    public boolean isLoggedIn() {
        Employee employee = getEmployeeBySession();
        try {
            if (employee != null) {
                if (Arrays.equals(getHash(employee), employee.getSessionHash())) {
                    // This extends the cache lifespan.
                    saveEmployeeToCache(employee);
                    return true;
                }
            } else {
                Logger.debug("Got null from getEmployeeBySession()");
            }
        } catch (Exception e) {
            Logger.debug("Exception caught: " + e.getClass().getName() + ":" + e.getMessage());
        }

        return false;
    }

    public void saveEmployeeToCache(Employee employee) {
        cache.set(sessionNamespace + session(sessionName), employee, sessionLifeSpan);
    }

    public Employee getEmployeeBySession() {
        return cache.get(sessionNamespace + session(sessionName));
    }

    private byte[] getHash(Employee employee) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(employee.getEmail().getBytes("UTF-8"));
        md.update(request().remoteAddress().getBytes("UTF-8"));
        md.update(configuration.getString("play.crypto.secret").getBytes());

        return md.digest();
    }

    public void destroySession() {
        Employee employee = getEmployeeBySession();

        if (employee != null) {
            cache.remove(sessionNamespace + session(sessionName));
        }

        session().remove(sessionName);
    }

    public void setSession(Employee employee) {
        try {
            SecureRandom secureRandom = new SecureRandom();
            byte[] seed = new byte[20];

            secureRandom.nextBytes(seed);
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(employee.getEmail().getBytes("UTF-8"));
            md.update(seed);

            String hexDigest = Hexadecimal.encodeBytesToString(md.digest());

            session().put(sessionName, hexDigest);
            employee.setSessionHash(getHash(employee));
            cache.set(sessionNamespace + hexDigest, employee, sessionLifeSpan);

        } catch (Exception e) {
            Logger.debug("EXCEPTION EXCEPTION EXCEPTION EXCEPTION!!!!");
        }

        return;
    }
}
