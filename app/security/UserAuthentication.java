package security;

import controllers.routes;
import models.Employee;
import play.Logger;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import security.authentication.GroupAuth;
import security.authentication.IsLoggedIn;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.session;

public class UserAuthentication extends Action<IsLoggedIn> {
    @Inject
    private Authentication authentication;

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        if (!authentication.isLoggedIn() || authentication.getEmployeeBySession() == null) {
            authentication.destroySession();
            return CompletableFuture.completedFuture(redirect(routes.LoginController.login()));
        }
        return delegate.call(ctx);
    }
}