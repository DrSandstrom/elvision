package security;

import controllers.routes;
import models.Employee;
import models.Group;
import play.Logger;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import security.authentication.GroupAuth;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Http.Context.Implicit.session;

public class GroupAuthentication extends Action<GroupAuth> {
    @Inject
    private Group group;

    @Inject
    private Authentication authentication;

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        int requiredAccessLevel = group.getAccessLevel(configuration.value());
        Employee employee = authentication.getEmployeeBySession();

        Logger.debug("Required access level: " + requiredAccessLevel);

        if (employee == null) {
            Logger.error("Employee object was null, should not be possible.");
            Logger.error("Redirecting to login-page");
            return CompletableFuture.completedFuture(redirect(routes.LoginController.login()));
        }

        int userAccessLevel = group.getAccessLevel(employee.getGroupName());
        Logger.debug("User access level: " + userAccessLevel);

        if (requiredAccessLevel > userAccessLevel) {
            Logger.debug("VALUE: " + configuration.value());
            return CompletableFuture.completedFuture(redirect(routes.ErrorController.unAuth()));
        }
        return delegate.call(ctx);
    }
}
