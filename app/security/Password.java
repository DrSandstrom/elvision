package security;

import models.Employee;
import play.Logger;

import java.security.MessageDigest;
import java.util.Arrays;

/**
 * Created by hannes on 2016-09-19.
 */
public class Password {

    private static final String HashingAlgorithm = "SHA-256";

    public static boolean compare(Employee employee, String password) {
        byte[] digest = create(password);

        if (Arrays.equals(employee.getPasshash(), digest) == true) {
            return true;
        }

        return false;
    }

    public static byte[] create(String password) {
        byte[] digest;

        try {
            MessageDigest md = MessageDigest.getInstance(HashingAlgorithm);
            md.update(password.getBytes("UTF-8"));
            digest = md.digest();
        } catch (Exception e) {
            Logger.error("Caught exception: " + e.getClass().getName() + ": " + e.getMessage());
            return null;
        }

        return digest;
    }


}
