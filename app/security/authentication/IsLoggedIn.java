package security.authentication;

/**
 * Created by Hannes on 2016-09-24.
 */
import play.mvc.With;
import security.UserAuthentication;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@With(UserAuthentication.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsLoggedIn {
}
